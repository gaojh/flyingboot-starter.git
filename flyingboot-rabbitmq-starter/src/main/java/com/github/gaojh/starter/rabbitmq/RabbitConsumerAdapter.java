package com.github.gaojh.starter.rabbitmq;

/**
 * @author gaojh
 * @since 2020/4/15
 */
public interface RabbitConsumerAdapter {

    void onMessage(String consumerTag, String message) throws Exception;
}
