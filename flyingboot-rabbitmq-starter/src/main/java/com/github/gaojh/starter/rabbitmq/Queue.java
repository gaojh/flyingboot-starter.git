package com.github.gaojh.starter.rabbitmq;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * @author gaojh
 * @since 2020/4/14
 */
@Data
@Accessors(chain = true)
public class Queue implements Serializable {

    /**
     * 队列名称
     */
    private String queue;

    /**
     * 是否持久化
     */
    private boolean durable = true;

    /**
     * 是否独占网络
     */
    private boolean exclusive = false;

    /**
     * 是否自动销毁
     */
    private boolean autoDelete = false;

    /**
     * 其他参数
     */
    private Map<String, Object> arguments;

    public Queue(String queue) {
        this.queue = queue;
    }

    public Queue(String queue, boolean durable) {
        this.queue = queue;
        this.durable = durable;
    }
}
