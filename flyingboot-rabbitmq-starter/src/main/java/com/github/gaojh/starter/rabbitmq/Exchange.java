package com.github.gaojh.starter.rabbitmq;

import com.rabbitmq.client.BuiltinExchangeType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * @author gaojh
 * @since 2020/4/14
 */
@Data
@Accessors(chain = true)
public class Exchange implements Serializable {

    private String exchange;
    private String type;
    private boolean durable = true;
    private boolean autoDelete = false;
    private boolean internal = false;
    private Map<String, Object> arguments;

    public Exchange(String exchange) {
        this.exchange = exchange;
        this.type = BuiltinExchangeType.DIRECT.getType();
    }

    public Exchange(String exchange, String type) {
        this.exchange = exchange;
        for (BuiltinExchangeType exchangeType : BuiltinExchangeType.values()) {
            if (exchangeType.getType().equalsIgnoreCase(type)) {
                this.type = exchangeType.getType();
            }
        }

        if (this.type == null) {
            throw new RuntimeException("不支持的Rabbitmq交换机类型：" + type);
        }
    }

    public Exchange(String exchange, BuiltinExchangeType type) {
        this.exchange = exchange;
        this.type = type.getType();
    }
}
