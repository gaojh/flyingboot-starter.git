package com.github.gaojh.starter.rabbitmq;

import com.github.gaojh.ioc.annotation.Component;
import com.github.gaojh.ioc.annotation.Value;
import lombok.Data;

import java.io.Serializable;

/**
 * @author gaojh
 * @since 2020/4/14
 */
@Component
@Data
public class RabbitProperties implements Serializable {

    @Value("${rabbitmq.host}")
    private String host;

    @Value("${rabbitmq.port}")
    private Integer port;

    @Value("${rabbitmq.virtual-host}")
    private String virtualHost;

    @Value("${rabbitmq.username}")
    private String username;

    @Value("${rabbitmq.password}")
    private String password;
}
