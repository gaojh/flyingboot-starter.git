package com.github.gaojh.starter.rabbitmq;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * @author gaojh
 * @since 2020/4/15
 */
@Data
@Accessors(chain = true )
public class Binding implements Serializable {

    private Queue queue;
    private Exchange exchange;
    private String routingKey;
    private Map<String, Object> arguments;
}
