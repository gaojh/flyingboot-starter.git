package com.github.gaojh.starter.rabbitmq.annotation;

import com.github.gaojh.ioc.annotation.Component;

import java.lang.annotation.*;

/**
 * @author gaojh
 * @since 2020/4/15
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Target(ElementType.TYPE)
@Component
public @interface RabbitConsumer {
    String queue();
    boolean autoAck() default false;
    String consumerTag() default "";
    boolean noLocal() default false;
    boolean exclusive() default false;

}
