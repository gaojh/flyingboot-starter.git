package com.github.gaojh.starter.feign.annotation;

import java.lang.annotation.*;

/**
 * @author gaojh
 * @since 2020/7/29
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FeignClient {

    String baseUrl() default "";
    String client() default "";
    String fallback() default "";
    String loadbalancer() default "";
    int connectTimeout() default 0;
    int readTimeout() default 0;
}
